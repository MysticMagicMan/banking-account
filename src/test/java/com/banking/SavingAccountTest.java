package com.banking;

import com.banking.account.SavingsAccount;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SavingAccountTest extends AccountTest{

    SavingsAccount savingsAccount;

    @Before
    public void init() {
        savingsAccount = new SavingsAccount("John", BigDecimal.valueOf(100), BigDecimal.valueOf(0.07));
    }

    @Test
    public void setInterestRate() {
        savingsAccount.setInterestRate(BigDecimal.valueOf(0.15));
        assertEquals(BigDecimal.valueOf(0.15), savingsAccount.getInterestRate());
    }

    @Test
    public void provideInterest() {
        savingsAccount.provideInterest();
        assertEquals(BigDecimal.valueOf(10700, 2), savingsAccount.getBalance().setScale(2));
    }


}
