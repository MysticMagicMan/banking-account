package com.banking;

import com.banking.account.CheckingAccount;
import com.banking.account.SavingsAccount;
import com.banking.exceptions.OverdraftLimitReachedException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CheckingAccountTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    CheckingAccount checkingAccount;
    SavingsAccount savingsAccount;

    @Before
    public void init() {
        checkingAccount = new CheckingAccount("Jane", BigDecimal.valueOf(200), BigDecimal.valueOf(100));
        savingsAccount = new SavingsAccount("John", BigDecimal.valueOf(100), BigDecimal.valueOf(0.07));
    }

    @Test
    public void transferMoney() {
        checkingAccount.transferMoney(savingsAccount, BigDecimal.valueOf(150));
        assertEquals(BigDecimal.valueOf(250), savingsAccount.getBalance());
        assertEquals(BigDecimal.valueOf(50), checkingAccount.getBalance());
    }

    @Test
    public void withdrawMoney() {
        checkingAccount.withdrawMoney(BigDecimal.valueOf(250));
        assertEquals(BigDecimal.valueOf(-50), checkingAccount.getBalance());

        exceptionRule.expect(OverdraftLimitReachedException.class);
        checkingAccount.withdrawMoney(BigDecimal.valueOf(100));
    }
}
