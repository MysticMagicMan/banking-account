package com.banking;

import com.banking.account.Account;
import com.banking.exceptions.InsuffiecentFundsException;
import com.banking.exceptions.NegativeAmountException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class AccountTest {
    Account account;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void init() {
        account = new Account("Jack", BigDecimal.valueOf(400));
    }

    @Test
    public void depositMoney() {
        account.depositMoney(BigDecimal.valueOf(200));
        assertEquals(BigDecimal.valueOf(600), account.getBalance());

        exceptionRule.expect(NegativeAmountException.class);
        account.depositMoney(BigDecimal.valueOf(-100));

    }

    @Test
    public void withdrawMoney() {

        account.withdrawMoney(BigDecimal.valueOf(150));
        assertEquals(BigDecimal.valueOf(250), account.getBalance());

        exceptionRule.expect(InsuffiecentFundsException.class);
        account.withdrawMoney(BigDecimal.valueOf(300));
    }



}
