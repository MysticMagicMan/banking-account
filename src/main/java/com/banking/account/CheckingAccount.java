package com.banking.account;

import com.banking.exceptions.InsuffiecentFundsException;
import com.banking.exceptions.OverdraftLimitReachedException;

import java.math.BigDecimal;

public class CheckingAccount extends Account {

    private BigDecimal overdraftLimit;

    public CheckingAccount(String user, BigDecimal balance, BigDecimal overdraftLimit) {
        super(user, balance);
        this.overdraftLimit = overdraftLimit;
    }

    public void withdrawMoney(BigDecimal amount) {
        if (isAmountOverLimit(amount)) {
            setBalance(getBalance().subtract(amount));
        }
    }

    public void transferMoney(Account transferAccountHolder, BigDecimal amount) {
        if (isAmountOverLimit(amount)){
            setBalance(getBalance().subtract(amount));
            transferAccountHolder.depositMoney(amount);
        }
    }

    private boolean isAmountOverLimit(BigDecimal amount) {
        if (amount.compareTo(getBalance().add(overdraftLimit)) < 0) {
            return true;
        }
        else {
            throw new OverdraftLimitReachedException("Overdraft limit reached for the account", null);
        }
    }

}
