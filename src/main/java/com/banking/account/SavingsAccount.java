package com.banking.account;

import com.banking.account.Account;

import java.math.BigDecimal;

public class SavingsAccount extends Account {

    private BigDecimal interestRate;

    public SavingsAccount(String user, BigDecimal balance, BigDecimal interestRate) {
        super(user, balance);
        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public void provideInterest() {
        depositMoney(getBalance().multiply(interestRate).setScale(2));
    }


}