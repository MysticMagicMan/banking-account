package com.banking.account;


import com.banking.exceptions.InsuffiecentFundsException;
import com.banking.exceptions.NegativeAmountException;

import java.math.BigDecimal;

public class Account {

    private String user;
    private BigDecimal balance;

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Account(String user, BigDecimal balance) {
        this.user = user;
        this.balance = balance;
    }

    public void depositMoney(BigDecimal amount) {
        isAmountNegative(amount);
        balance = balance.add(amount);
    }

    public void withdrawMoney(BigDecimal amount) {
        isAmountNegative(amount);
        if (balance.compareTo(amount) >= 0) {
            balance = balance.subtract(amount);
        }
        else {
            throw new InsuffiecentFundsException("Insufficient funds on bank account", null);
        }
    }

    private void isAmountNegative(BigDecimal amount) {
        if (amount.signum() == -1){
            throw new NegativeAmountException("Withdrawal or deposit amount cannot be negative", null);
        }
    }
}

