package com.banking.exceptions;

public class OverdraftLimitReachedException extends RuntimeException{

    public OverdraftLimitReachedException(String errorMessage) {
        super(errorMessage);
    }
}
