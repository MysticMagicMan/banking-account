package com.banking.exceptions;

public class NegativeAmountException extends RuntimeException{

    public NegativeAmountException(String errorMessage) {
        super(errorMessage);
    }
}
