package com.banking.exceptions;

public class InsuffiecentFundsException extends RuntimeException {

    public InsuffiecentFundsException(String errorMessage) {
        super(errorMessage);
    }
}

